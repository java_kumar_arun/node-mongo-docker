## open your shell/terminal
## move to the directory where docker-compose.yml is present

**$ docker-compose build**

### once build step is done

**$ docker-compose up**

### find ip address on which your container is running:
**$ docker inspect containerId**

#### to find containerId in previous command
**$ docker ps**

###### Note: If you are using windows OS, you can use kitematic web view to find container IP address.

###### Note: If you are using windows OS, you can use gitbash
**docker inspect containerId | grep "IPAddress"

