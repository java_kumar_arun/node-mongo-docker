var express = require('express');
var router = express.Router();

var Employee = require('./../models/Employee');


var employee_controllers = require("../controllers/EmployeeController.js");

// Get all employees
router.get('/employees', function(req, res) {
    Employee.find({}).exec(function (err, employees) {
      if (err) {
        console.log("Error:", err);
        res.json({success: false});
      }
      else {
        res.json(employees);
      }
    });
  });

router.post('/employees', employee_controllers.save);

module.exports = router;