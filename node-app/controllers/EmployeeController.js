var Employee = require('./../models/Employee');


const EmployeeControllers = {
    list: function(req, res) {
        Employee.find({}).exec(function (err, employees) {
          if (err) {
            console.log("Error:", err);
            res.json({success: false});
          }
          else {
            res.json(employees);
          }
        });
      },

      save: function(req, res) {
        var employee = new Employee(req.body);
      
        employee.save(function(err) {
          if(err) {
            console.log(err);
            res.json({success: false});
          } else {
            console.log("Successfully created an employee.");
            res.json({success: true});
          }
        });
    }
}

module.exports = EmployeeControllers;