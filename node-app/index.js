var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const dbUrl = '192.168.99.100:27017';
// in your case find ip address of the mongo container, port will be same as given in docker-compose.yml file

mongoose.connect(`mongodb://${dbUrl}/mydb`, { useNewUrlParser: true })
    .then(() => console.log('connection succesful'))
    .catch((err) => console.error(err));


var app = express();
app.use(bodyParser.json());

var employees = require('./routes/employees');
app.use(express.static('public'));
app.use('/api/v1', employees);

app.set('port', (process.env.PORT || 5000));
app.listen(app.get('port'), (err)=> {
    if (err) {
        console.log("Error in running server");
        return err;
    }
    console.log("Node app is running at " + app.get('port'))
})