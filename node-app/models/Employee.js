var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EmpSchema = new Schema({
    name: String,
    address: String,
    salary: Number,
    joining_date: {type: Date, default: Date.now }
});

module.exports = mongoose.model("Employee", EmpSchema);